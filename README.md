STM32_Final_Project: Temperature Control of Cold Room Implementing an MQTT communication with the ESP32

Author.
Daniel Esteban Parada Alvarez
daniel.parada@utp.edu.co


This part belongs to receiving the data by UART from the STM32, and sending the data through MQTT
Project description
This will be a temperature reading system.
There will be two interfaces, AWS website, and STM32's LCD.
The LCD will show the current temperature (every 5 seconds).
The LCD will have a button that will turn on/off a GPIO (anytime).
