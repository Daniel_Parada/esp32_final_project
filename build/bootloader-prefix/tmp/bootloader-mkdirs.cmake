# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "C:/Users/Daniel/.stm32cubeide/esp-idf/components/bootloader/subproject"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix/tmp"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix/src/bootloader-stamp"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix/src"
  "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "C:/Users/Daniel/Desktop/ESP32_Final_Poject/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
